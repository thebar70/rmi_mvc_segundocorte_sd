/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clienteObjetos;

import java.awt.event.ActionEvent;
import vistas.FrmPrincipal;

/**
 *
 * @author thebar70
 */
public class ClienteController implements java.awt.event.ActionListener {
    ClienteModelo modelo;
    FrmPrincipal vista;
    @Override
    public void actionPerformed(ActionEvent e) {
        switch(e.getActionCommand()){
            case "jTextFildAnteproyectoBuscado":
                modelo.buscarAnteproyecto(vista.getCodigoAnteproyectoBuscado());
                modelo.buscarEvaluador(vista.getCodigoAnteproyectoBuscado());
                break;
            case "jButtonListar":
                modelo.listaAnteproyectos();
                break;
            case "cambiarConceptoCodApBuscado":
                modelo.buscarAnteproyecto(vista.cambiar_concepto_getCondigoApBuscado());
                modelo.buscarEvaluador(vista.cambiar_concepto_getCondigoApBuscado());
                break;
            case "cambiarConceptoJbuttonGuardar":
                modelo.cambiarConcepto(vista.cambiar_concepto_getCondigoApBuscado(),
                        vista.getCedulaEvaluador(),
                        vista.cambiar_concepto_getConsepto());
                break;
            default:
                break;
        }
    }
    public void addModel(ClienteModelo m) {
        this.modelo = m;
    }

    public void addView(FrmPrincipal v) {
        this.vista = v;
    }
}
