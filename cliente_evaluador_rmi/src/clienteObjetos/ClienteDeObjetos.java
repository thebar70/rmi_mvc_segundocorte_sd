/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clienteObjetos;

import java.rmi.RemoteException;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import servidor.dto.UsuarioDTO;
import servidor.sop_rmi.CallbackInt;
import servidor.sop_rmi.CallbackImpl;
import vistas.FrmPrincipal;
import servidor.sop_rmi.GestionAnteproyectosInt;
import servidor.sop_rmi.GestionUsuariosInt;
import vistas.BuscarAnteproyecto;
import vistas.CambiarConceptoAnteproyecto;
import vistas.ListaAnteproyectos;

/**
 *
 * @author thebar70
 */
public class ClienteDeObjetos {

    private static GestionUsuariosInt objRemotoUser;
    private static GestionAnteproyectosInt objRemotoAP;
    private static CallbackInt objRemotoCallBack ;

    public static void main(String arg[]) throws RemoteException {
        int numPuertoRMIRegistry;
        String direccionIpRMIRegistry;

        //System.out.println("Cual es la direccion ip donde se encuentra  el rmiregistry ");
        direccionIpRMIRegistry = "localhost";
        //System.out.println("Cual es el numero de puerto por el cual escucha el rmiregistry ");
        numPuertoRMIRegistry = 2020;
        
        objRemotoUser = (GestionUsuariosInt) utilidades.UtilidadesRegistroC.obtenerObjRemoto(direccionIpRMIRegistry, numPuertoRMIRegistry, "ObjetoRemotoUsuarios");
        
        ValidacionCredenciales validacion = new ValidacionCredenciales(objRemotoUser);
        System.out.println("Se realizo conexion con exito"); 
        if (validacion.validarCredenciales()) {
            
            objRemotoCallBack = new CallbackImpl();
            objRemotoAP = (GestionAnteproyectosInt) utilidades.UtilidadesRegistroC.obtenerObjRemoto(direccionIpRMIRegistry, numPuertoRMIRegistry, "ObjetoRemotoAnteproyectos");
            objRemotoAP.registrarRefEvaludor(objRemotoCallBack);
            
            

            ClienteModelo modelo = new ClienteModelo(objRemotoAP);
            ClienteController controller = new ClienteController();
            FrmPrincipal myView = new FrmPrincipal(validacion.getNombreUsuario(),
                    getCedulaUsuario(validacion.getNombreUsuario()));

            BuscarAnteproyecto panelBuscarAnteproyecto = new BuscarAnteproyecto();
            CambiarConceptoAnteproyecto panelCambiarConcepto = new CambiarConceptoAnteproyecto();
            ListaAnteproyectos panelListaAnteproyectos = new ListaAnteproyectos();

            modelo.addObserver(myView);
            modelo.addObserver(panelBuscarAnteproyecto);
            modelo.addObserver(panelListaAnteproyectos);
            modelo.addObserver(panelCambiarConcepto);

            controller.addModel(modelo);
            controller.addView(myView);

            myView.addController(controller);
            panelBuscarAnteproyecto.addController(controller);
            panelListaAnteproyectos.addController(controller);
            panelCambiarConcepto.addController(controller);

            myView.agregarPaneles(panelBuscarAnteproyecto, panelCambiarConcepto, panelListaAnteproyectos);
            myView.setVisible(true);

        } else {
            JOptionPane.showMessageDialog(null, "Error al vaidar las credenciales");
        }

    }
    private static int  getCedulaUsuario(String nombre_usuario){
        int cedula=0;
        try {
            ArrayList<UsuarioDTO> lista_usuarios=objRemotoUser.listaUsuarios();
            for(UsuarioDTO user: lista_usuarios){
                if(user.getUsuario().equals(nombre_usuario)){
                    cedula=user.getNumIdentificacion();
                }
            }
        } catch (RemoteException e) {
            System.out.println("Error al trar lista de usuarios\n"+e);
        }
        return cedula;
    }
}
