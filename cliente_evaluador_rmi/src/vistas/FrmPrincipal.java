/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vistas;

import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;

/**
 *
 * @author thebar70
 */
public class FrmPrincipal extends javax.swing.JFrame implements Observer{

    /**
     * Creates new form FrmPrincipal
     */
    CambiarConceptoAnteproyecto panelCambiarConcepto;
    BuscarAnteproyecto panelBuscarAnteproyecto;
    ListaAnteproyectos panelListarAnteproyectos;
    int cedula;
    
    public FrmPrincipal(String usuario, int cedula) {
        this.cedula=cedula;
        initComponents();
        saludar(usuario);
    }
    private void saludar(String nombreUsuaior){
        this.jPanelPrincipal.removeAll();
        panelSaludo saludo=new panelSaludo();
        saludo.setMensaje("Bienvenido "+nombreUsuaior);
        this.jPanelPrincipal.add(saludo);
        this.jPanelPrincipal.updateUI();
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanelPrincipal = new javax.swing.JPanel();
        jButtonBuscar = new javax.swing.JButton();
        jButtonListar = new javax.swing.JButton();
        jButtonCambbiarConcepto = new javax.swing.JButton();
        jButtonSalir = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanelPrincipal.setBackground(new java.awt.Color(204, 204, 255));
        jPanelPrincipal.setLayout(new java.awt.CardLayout());
        getContentPane().add(jPanelPrincipal, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 20, 540, 360));

        jButtonBuscar.setText("BuscarAnteproyecto");
        jButtonBuscar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButtonBuscarMouseClicked(evt);
            }
        });
        getContentPane().add(jButtonBuscar, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 130, -1, -1));

        jButtonListar.setText("ListarAnteproyestos");
        jButtonListar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButtonListarMouseClicked(evt);
            }
        });
        getContentPane().add(jButtonListar, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 170, -1, -1));

        jButtonCambbiarConcepto.setText("CambiarConcepto");
        jButtonCambbiarConcepto.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButtonCambbiarConceptoMouseClicked(evt);
            }
        });
        getContentPane().add(jButtonCambbiarConcepto, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 210, -1, -1));

        jButtonSalir.setText("Salir");
        jButtonSalir.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButtonSalirMouseClicked(evt);
            }
        });
        getContentPane().add(jButtonSalir, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 340, -1, -1));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/files/perfil.png"))); // NOI18N
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 20, 130, 80));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonBuscarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonBuscarMouseClicked
        this.jPanelPrincipal.removeAll();
        this.jPanelPrincipal.add(panelBuscarAnteproyecto);
        this.jPanelPrincipal.updateUI();
    }//GEN-LAST:event_jButtonBuscarMouseClicked

    private void jButtonListarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonListarMouseClicked
        this.jPanelPrincipal.removeAll();
        this.jPanelPrincipal.add(panelListarAnteproyectos);
        this.jPanelPrincipal.updateUI();
    }//GEN-LAST:event_jButtonListarMouseClicked

    private void jButtonCambbiarConceptoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonCambbiarConceptoMouseClicked
        this.jPanelPrincipal.removeAll();
        panelCambiarConcepto.limpiarPanel();
        this.jPanelPrincipal.add(panelCambiarConcepto);
        this.jPanelPrincipal.updateUI();
    }//GEN-LAST:event_jButtonCambbiarConceptoMouseClicked

    private void jButtonSalirMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonSalirMouseClicked
        System.exit(0);
    }//GEN-LAST:event_jButtonSalirMouseClicked

    @Override
    public void update(Observable o, Object arg) {
        System.out.println("Todo actualizar");
    }
    public void addController(ActionListener controller) {
         System.out.println("todo addcontroller");
    }

    public void agregarPaneles(BuscarAnteproyecto panelBuscarAnteproyecto,
            CambiarConceptoAnteproyecto panelCambiarConcepto,
            ListaAnteproyectos panelLista) {
        this.panelBuscarAnteproyecto=panelBuscarAnteproyecto;
        this.panelCambiarConcepto=panelCambiarConcepto;
        this.panelListarAnteproyectos=panelLista;
        this.panelCambiarConcepto.setCedula(cedula);
    }
    public int getCodigoAnteproyectoBuscado() {
        return panelBuscarAnteproyecto.getCodigoAnteproyectoBuscado();
    }
    
    public int cambiar_concepto_getCondigoApBuscado(){
        return panelCambiarConcepto.getCodigoAnteproyectoBuscado();
    }
    public int cambiar_concepto_getConsepto(){
        return panelCambiarConcepto.getNuevoConcepto();
    }
    public int getCedulaEvaluador(){
        return this.cedula;
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonBuscar;
    private javax.swing.JButton jButtonCambbiarConcepto;
    private javax.swing.JButton jButtonListar;
    private javax.swing.JButton jButtonSalir;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanelPrincipal;
    // End of variables declaration//GEN-END:variables

    
}
