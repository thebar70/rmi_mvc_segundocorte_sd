/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clienteObjetos;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Observable;
import servidor.dto.AnteproyectoDTO;
import servidor.dto.EvaluadorDTO;
import servidor.sop_rmi.GestionAnteproyectosInt;

/**
 *
 * @author thebar70
 */
public class ClienteModelo extends Observable{
    private int identidicafor;
    private AnteproyectoDTO anteproyecto;
    private ArrayList<AnteproyectoDTO> anteproyectos;
    GestionAnteproyectosInt objRemotoAP;
    
    public ClienteModelo(GestionAnteproyectosInt objRemotoAP){
        this.objRemotoAP=objRemotoAP;
    }
    /**
     * Identificado de funcion 1
     */
    public void listaAnteproyectos(){
        try {
            anteproyectos=objRemotoAP.listarAnteproyectos();
            identidicafor=1;
            setChanged();
            notifyObservers(this);
            
        } catch (RemoteException e) {
            System.out.println("Error al consultar la lista de anteproyectos\n"+e);
        }
    }
    /**
     * Identificado de funcion 2
     * @param codigoAp
     */
    public void buscarAnteproyecto(int codigoAp){
        try {
            anteproyecto=objRemotoAP.buscarAnteproyecto(codigoAp);
            identidicafor=2;
            setChanged();
            notifyObservers(this);
            
        } catch (RemoteException e) {
            System.out.println("Error al buscar el anteproyecto\n"+e);
        }
    }
    /**
     * Identificador de funcion 3
     * @param codigoAp
     */
    public void buscarEvaluador(int codigoAp){
        try {
            evaluador=objRemotoAP.buscarEvaluadores(codigoAp);
            identidicafor=3;
            setChanged();
            notifyObservers(this);
            
        } catch (RemoteException e) {
            System.out.println("Error al buscar el anteproyecto\n"+e);
        }
    }
    
    
    
    public ArrayList<AnteproyectoDTO> getAnteproyectos() {
        return anteproyectos;
    }
    private EvaluadorDTO evaluador;

    public int getIdentidicafor() {
        return identidicafor;
    }

    public AnteproyectoDTO getAnteproyecto() {
        return anteproyecto;
    }
    public EvaluadorDTO getEvaluador(){
        return evaluador;
    }
    
    
    
}
