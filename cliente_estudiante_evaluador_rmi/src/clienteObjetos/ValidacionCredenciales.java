/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clienteObjetos;

import login.Credenciales;
import login.FrmInicioSesion;
import javax.swing.JOptionPane;
import servidor.dto.IniciarSesionDTO;
import servidor.sop_rmi.GestionUsuariosInt;

/**
 *
 * @author thebar70
 */
public class ValidacionCredenciales {
    Credenciales cred;
    private static GestionUsuariosInt objRemotoUser;

    public ValidacionCredenciales(GestionUsuariosInt objRemotoUser) {
        this.objRemotoUser = objRemotoUser;
    }

    public boolean validarCredenciales() {
        
        FrmInicioSesion lg = new FrmInicioSesion();
        lg.setVisible(true);
        boolean esCorrecta = false;

        while (esCorrecta == false) {
            cred = lg.crendeciales(false);
            try {
                System.out.println("nombre:"+cred.getUsuario());
                System.out.println("pass:"+cred.getContraseña());
                IniciarSesionDTO objIS = new IniciarSesionDTO(cred.getUsuario(), cred.getContraseña());
        
                int tipo=objRemotoUser.iniciarSesion((IniciarSesionDTO) objIS);
                System.out.println("tipo: "+tipo);
                if (tipo>0) {
                    if(tipo!=2){
                        esCorrecta = false;
                        JOptionPane.showMessageDialog(null, "Usted no esta autorizado para iniciar sesion en esta terminal");
                    }else{
                        esCorrecta = true;
                        System.out.println("Contraseña correcta");
                    }

                } else {
                    lg.sacudir();
                    esCorrecta = false;
                }

            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "No se pudieron validar las credenciales\n"+e);
            }

        }
        lg.dispose();
        return true;

    }
    public void iniciarSesion(){
        
    }

    public String getNombreUsuario() {
        return cred.getUsuario();
    }

}
