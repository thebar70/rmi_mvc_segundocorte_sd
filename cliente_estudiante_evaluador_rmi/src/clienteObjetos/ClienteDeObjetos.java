/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clienteObjetos;

import javax.swing.JOptionPane;
import servidor.sop_rmi.GestionAnteproyectosInt;
import servidor.sop_rmi.GestionUsuariosInt;
import vistas.BuscarAnteproyecto;
import vistas.FrmPrincipal;
import vistas.ListaAnteproyectos;

/**
 *
 * @author thebar70
 */
public class ClienteDeObjetos {

    private static GestionUsuariosInt objRemotoUser;
    private static GestionAnteproyectosInt objRemotoAP;

    public static void main(String arg[]) {
        int numPuertoRMIRegistry;
        String direccionIpRMIRegistry;

        //System.out.println("Cual es la direccion ip donde se encuentra  el rmiregistry ");
        direccionIpRMIRegistry = "localhost";
        //System.out.println("Cual es el numero de puerto por el cual escucha el rmiregistry ");
        numPuertoRMIRegistry = 2020;

        objRemotoUser = (GestionUsuariosInt) utilidades.UtilidadesRegistroC.obtenerObjRemoto(direccionIpRMIRegistry, numPuertoRMIRegistry, "ObjetoRemotoUsuarios");

        ValidacionCredenciales validacion = new ValidacionCredenciales(objRemotoUser);
        System.out.println("Se realizo conexion con exito");
        if (validacion.validarCredenciales()) {
            objRemotoAP = (GestionAnteproyectosInt) utilidades.UtilidadesRegistroC.obtenerObjRemoto(direccionIpRMIRegistry, numPuertoRMIRegistry, "ObjetoRemotoAnteproyectos");
            
            ClienteModelo modelo = new ClienteModelo(objRemotoAP);
            ClienteController controller = new ClienteController();
            FrmPrincipal  myView = new FrmPrincipal(validacion.getNombreUsuario());

            BuscarAnteproyecto panelBuscarAnteproyecto = new BuscarAnteproyecto();
            ListaAnteproyectos panelListaAnteproyectos = new ListaAnteproyectos();
            

            modelo.addObserver(myView);
            modelo.addObserver(panelListaAnteproyectos);
            modelo.addObserver(panelBuscarAnteproyecto);
            

            controller.addModel(modelo);
            controller.addView(myView);
            

            myView.addController(controller);
            panelBuscarAnteproyecto.addController(controller);
            panelListaAnteproyectos.addController(controller);

            myView.agregarPaneles(panelBuscarAnteproyecto,panelListaAnteproyectos);
            myView.setVisible(true);

        } else {
            JOptionPane.showMessageDialog(null, "Error al vaidar las credenciales");
        }

    }

}
