/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cliente.vistas.VistasJefe;

import cliente.clienteObjetos.clienteModelo;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;
import javax.swing.JOptionPane;
import servidor.dto.AnteproyectoDTO;
import servidor.dto.EvaluadorDTO;

/**
 *
 * @author thebar70
 */
public class ModificarEstadoAnteproyecto extends javax.swing.JPanel implements Observer {

    /**
     * Creates new form CambiarConceptoAnteproyecto
     */
    AnteproyectoDTO anteproyecto;
    EvaluadorDTO evaluador;

    public ModificarEstadoAnteproyecto() {
        initComponents();
        this.jTextFieldCodigoAnteproyecto.setActionCommand("CambiarConceptoAnteproyectoJtextFieldCodigoAp");
        this.jButtonModificar.setActionCommand("CambiarConceptoAnteproyectoJbuttonModificar");
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTextFieldCodigoAnteproyecto = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        jButtonModificar = new javax.swing.JButton();

        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        add(jTextFieldCodigoAnteproyecto, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 30, 140, -1));

        jLabel1.setText("Codigo Anteproyecto");
        add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 10, -1, -1));

        jTextArea1.setBackground(new java.awt.Color(204, 204, 204));
        jTextArea1.setColumns(20);
        jTextArea1.setRows(5);
        jScrollPane1.setViewportView(jTextArea1);

        add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 60, 460, 170));

        jButtonModificar.setText("Modificar");
        add(jButtonModificar, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 240, -1, -1));
    }// </editor-fold>//GEN-END:initComponents
    
    @Override
    public void update(Observable o, Object arg) {
        clienteModelo model = (clienteModelo) arg;
        if (model.getIdentificador() == 4) {
            anteproyecto = model.getAnteproyectoDTO();
            if (anteproyecto != null) {
                pintarInformacionAnteproyecto(anteproyecto);
            } else {
                this.jTextArea1.setText("****No se encontro anteporyecto****");
            }
        }
        if (model.getIdentificador() == 5) {
            evaluador = model.getEvaluadorDTO();
            if (evaluador != null) {
                pintarInformacionEvaluador(evaluador);
            }
        }
        if(model.getIdentificador()==7){
            JOptionPane.showMessageDialog(null, "Se cambio correctamnete el conceto del anteproyecto");
        }
    }
    private void limpiar(){
        this.jTextArea1.setText("");
        this.jTextFieldCodigoAnteproyecto.setText("");
    }

    private void pintarInformacionEvaluador(EvaluadorDTO evaluador) {
        if (anteproyecto != null) {
            if (evaluador != null) {
                
                 this.jTextArea1.append("\nEvaluador 1: " + evaluador.getNombreEvaluador_1() + "\n");
                if (evaluador.getConceptoEval_1() == 1) {
                     this.jTextArea1.append("Concepto: Aprobadoņ\n");
                     this.jButtonModificar.setEnabled(true);
                } else {
                     this.jTextArea1.append("Concepto: No Aprobado\n");
                     
                }
                this.jTextArea1.append("\nEvaluador 2: " + evaluador.getNombreEvaluador_2() + "\n");
                if (evaluador.getConceptoEval_2() == 1) {
                     this.jTextArea1.append("Concepto: Aprobado\n");
                } else {
                     this.jTextArea1.append("Conceto: No Aprobado\n");
                     this.jButtonModificar.setEnabled(false);
                }
            } else {
                this.jTextArea1.append("***Este anteproyecno no tiene evaluadores asignados***");
                this.jButtonModificar.setEnabled(false);
            }
        }
    }

    private void pintarInformacionAnteproyecto(AnteproyectoDTO anteproyecto) {
        this.jTextArea1.setText("Titulo: " + anteproyecto.getTitulo() + "\n");
        this.jTextArea1.append("Estudiante 1: " + anteproyecto.getEstudiente_1() + "\n");
        this.jTextArea1.append("Estudiante 2: " + anteproyecto.getEstudainte_2() + "\n");
        this.jTextArea1.append("______________________________________________");
        this.jButtonModificar.setEnabled(false);

    }
    public int getCodigoAnteproyecto() {
        return anteproyecto.getCodigoAP();
    }

    public int getCodigoAnteproyectoBuscado() {
        return Integer.parseInt(this.jTextFieldCodigoAnteproyecto.getText());
    }

    public void addController(ActionListener controller) {
        this.jTextFieldCodigoAnteproyecto.addActionListener(controller);
        this.jButtonModificar.addActionListener(controller);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonModificar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JTextField jTextFieldCodigoAnteproyecto;
    // End of variables declaration//GEN-END:variables

}
