/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cliente.vistas.VistasJefe;

import cliente.clienteObjetos.clienteModelo;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;
import javax.swing.JOptionPane;
import servidor.dto.UsuarioDTO;

/**
 *
 * @author thebar70
 */
public class AgregarUsuario extends javax.swing.JPanel implements Observer{

    /**
     * Creates new form agregarUsuario
     */
    public AgregarUsuario() {
        initComponents();
        this.jButtonRegistrar.setActionCommand("panelAgregarUsuarioRegistrar");
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTextFieldNombre = new javax.swing.JTextField();
        jTextFieldIdentificacion = new javax.swing.JTextField();
        jTextFieldUsuario = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jTextFieldContraseña = new javax.swing.JTextField();
        jComboBoxTipo = new javax.swing.JComboBox<>();
        jButtonRegistrar = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();
        jSeparator4 = new javax.swing.JSeparator();

        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        add(jTextFieldNombre, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 60, 170, -1));
        add(jTextFieldIdentificacion, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 90, 170, -1));
        add(jTextFieldUsuario, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 120, 170, -1));

        jLabel1.setText("Agragar Nuevo Usuario");
        add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(84, 12, -1, -1));

        jLabel2.setText("Nombre");
        add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 60, -1, -1));

        jLabel3.setText("Contraseña");
        add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 160, -1, -1));

        jLabel4.setText("Tipo");
        add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 200, -1, -1));

        jLabel5.setText("Identificacion");
        add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 90, -1, -1));

        jLabel6.setText("Usuario");
        add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 120, -1, -1));
        add(jTextFieldContraseña, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 160, 170, -1));

        jComboBoxTipo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Tipo", "JefeDepartamento", "Estudiante/Director", "Evaluador" }));
        add(jComboBoxTipo, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 190, -1, -1));

        jButtonRegistrar.setText("Registrar");
        add(jButtonRegistrar, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 260, -1, -1));
        add(jSeparator1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 400, 10));

        jSeparator4.setOrientation(javax.swing.SwingConstants.VERTICAL);
        add(jSeparator4, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 10, 260));
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonRegistrar;
    private javax.swing.JComboBox<String> jComboBoxTipo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JTextField jTextFieldContraseña;
    private javax.swing.JTextField jTextFieldIdentificacion;
    private javax.swing.JTextField jTextFieldNombre;
    private javax.swing.JTextField jTextFieldUsuario;
    // End of variables declaration//GEN-END:variables

    @Override
    public void update(Observable o, Object arg) {
        clienteModelo model=(clienteModelo)arg;
        if(model.getIdentificador()==0){
            JOptionPane.showMessageDialog(null, "Se registro el usuario correctamnete");
            limpiar();
        }
    }
    public void addController(ActionListener controller) {
        this.jButtonRegistrar.addActionListener(controller);
    }
    public UsuarioDTO getNuevoUsuario(){
        String nombre=this.jTextFieldNombre.getText();
        int id=Integer.parseInt(this.jTextFieldIdentificacion.getText());
        String usuario=this.jTextFieldUsuario.getText();
        String contraseña=this.jTextFieldContraseña.getText();
        int tipo=this.jComboBoxTipo.getSelectedIndex();
        UsuarioDTO nuevo = new UsuarioDTO(nombre,id, usuario, contraseña, tipo);
        return nuevo;
    }
    private void limpiar(){
        this.jTextFieldContraseña.setText("");
        this.jTextFieldIdentificacion.setText("");
        this.jTextFieldNombre.setText("");
        this.jTextFieldUsuario.setText("");
        this.jComboBoxTipo.setSelectedIndex(0);
    }
}
