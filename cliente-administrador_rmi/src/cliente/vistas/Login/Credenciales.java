/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cliente.vistas.Login;

/**
 *
 * @author thebar70
 */
public class Credenciales {
    private String usuario;
    private char[] contraseña;

    public Credenciales(String usuario, char[] contraseña) {
        this.usuario = usuario;
        this.contraseña = contraseña;
    }
    
    
    
    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContraseña() {
        
        return String.valueOf(contraseña);
    }

    public void setContraseña(char[] contraseña) {
        this.contraseña = contraseña;
    }
    
}
