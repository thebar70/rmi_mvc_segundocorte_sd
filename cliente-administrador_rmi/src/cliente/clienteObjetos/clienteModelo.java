/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cliente.clienteObjetos;

import java.rmi.RemoteException;
import java.time.LocalDate;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import servidor.dto.AnteproyectoDTO;
import servidor.dto.EvaluadorDTO;
import servidor.dto.UsuarioDTO;
import servidor.sop_rmi.GestionAnteproyectosInt;
import servidor.sop_rmi.GestionUsuariosInt;

/**
 *
 * @author thebar70
 */
public class clienteModelo extends java.util.Observable {

    private static GestionAnteproyectosInt objRemotoAP;
    private static GestionUsuariosInt objRemotoUser;

    ArrayList<UsuarioDTO> usuarios;
    ArrayList<AnteproyectoDTO> anteproyectos;
    AnteproyectoDTO anteproyecto_buscado;
    EvaluadorDTO evaludaro_buscado;
    private int function_identifier = 0;
    boolean resultadoModificaion = false;

    public clienteModelo(GestionAnteproyectosInt objRemotoAP, GestionUsuariosInt objRemotoUser) {
        clienteModelo.objRemotoAP = objRemotoAP;
        clienteModelo.objRemotoUser = objRemotoUser;

    }

    /**
     * Identificador de funcion 0
     * @param nuevo
     */
    public void registrarUsuario(UsuarioDTO nuevo) {
        boolean resultado = false;
        try {
            if (objRemotoUser.registrarUsuario(nuevo)) {
                resultado = true;
                this.function_identifier = 0;
                setChanged();
                notifyObservers(this);
            } else {
                resultado = false;
                System.out.println("Errores, al registrar el usuario desde el servidor");
            }
        } catch (RemoteException e) {
            System.out.println("Error al conectar con server para registro\n" + e);
        }

    }

    /**
     * Identificador de funcion 1
     */
    public void ListaUsuarios() {
        try {
            this.usuarios = (ArrayList<UsuarioDTO>) objRemotoUser.listaUsuarios();
            this.function_identifier = 1;
            setChanged();
            notifyObservers(this);
        } catch (RemoteException e) {
            System.out.println("Error al crear la lista de usuarios\n" + e);
        }

    }

    /**
     * Identificador de funcion 2
     *
     * @param nuevo
     */
    public void registrarAnteproyecto(AnteproyectoDTO nuevo) {
        try {
            objRemotoAP.registrarAnteproyectos(nuevo);

            this.function_identifier = 2;
            setChanged();
            notifyObservers(this);
        } catch (RemoteException e) {
            System.out.println("Error al registrar el anteproyecto\n" + e);
        }

    }

    /**
     * Identificador de fucncion 3
     */
    public void listaAnteproyectos() {
        try {
            this.anteproyectos = objRemotoAP.listarAnteproyectos();
            this.function_identifier = 3;
            setChanged();
            notifyObservers(this);

        } catch (RemoteException e) {
            System.out.println("Error al consultar loa nateprotevctio\n " + e);
        }
    }

    /**
     * Funcion Identifier 4
     *
     * @param codigoAnteproyectoDTO
     */
    public void buscarAnteproyecto(int codigoAnteproyectoDTO) {
        try {
            anteproyecto_buscado = null;
            anteproyecto_buscado = objRemotoAP.buscarAnteproyecto(codigoAnteproyectoDTO);
            this.function_identifier = 4;
            setChanged();
            notifyObservers(this);

        } catch (RemoteException e) {
            System.out.println("Error, al buscar el anteproyecto\n" + e);
        }
    }

    /**
     * Funcion identifier=5
     *
     * @param codigoAp
     */
    public void buscarEvaluadorEnAnteproyecto(int codigoAp) {
        evaludaro_buscado = null;
        try {
            evaludaro_buscado = objRemotoAP.buscarEvaluadores(codigoAp);
            this.function_identifier = 5;
            setChanged();
            notifyObservers(this);

        } catch (RemoteException e) {
            System.out.println("Erro al buscar evaluador para el anteproyecto\n" + e);
        }
    }

    /**
     * Identificado de funcion 6
     */
    public void asignarEvaluador(int posEvaluador1, int posEvaluador2, int codigoAnteproyecto) {
        try {
            if (posEvaluador1 > -1 && posEvaluador2 > -1) {
                EvaluadorDTO nuevo = new EvaluadorDTO(codigoAnteproyecto,
                        usuarios.get(posEvaluador1).getNumIdentificacion(),
                        usuarios.get(posEvaluador1).getNombres(),
                        usuarios.get(posEvaluador2).getNumIdentificacion(),
                        usuarios.get(posEvaluador2).getNombres());
                nuevo.setFechaRevision_1(LocalDate.MIN);
                nuevo.setFechaRevision_2(LocalDate.MIN);
                objRemotoAP.asignarEvaluadores(nuevo);
                this.function_identifier = 6;
                setChanged();
                notifyObservers(this);
            } else {
                JOptionPane.showMessageDialog(null, "Debe seleccionar dos evaluadores y un anteproycto");
            }
        } catch (RemoteException e) {
            System.out.println("Error al asignar avaluador\n" + e);
        }

    }

    /**
     * Identificador de funcion 7
     *
     * @param codigoAnteproyecto
     */
    public void modificarConceptoAnteproyecto(int codigoAnteproyecto) {
        try {
            resultadoModificaion = objRemotoAP.modificarConcepto(codigoAnteproyecto);
            this.function_identifier = 7;
            setChanged();
            notifyObservers(this);
        } catch (RemoteException e) {
            System.out.println("Error al modificar el conecepto desde el jefe de deprtamento\n" + e);
        }
    }

    /*
     * Getters de los diferentes variables del modelo,
     * 
     */
    public boolean isModicado() {
        return resultadoModificaion;
    }

    public ArrayList<AnteproyectoDTO> getAnteproyectos() {
        return this.anteproyectos;
    }

    public ArrayList<UsuarioDTO> getUsuarios() {

        return this.usuarios;
    }

    public AnteproyectoDTO getAnteproyectoDTO() {
        return this.anteproyecto_buscado;
    }

    public EvaluadorDTO getEvaluadorDTO() {
        return this.evaludaro_buscado;

    }

    //Getter de la variable identificador que cambia conforme se llaman a los metodos del modelo 
    public int getIdentificador() {
        return this.function_identifier;
    }

}
