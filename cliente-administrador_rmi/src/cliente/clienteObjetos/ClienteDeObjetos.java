/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cliente.clienteObjetos;

import cliente.vistas.VistasJefe.AgregarAnteproyecto;
import cliente.vistas.VistasJefe.AsignarEvaluador;
import cliente.vistas.VistasJefe.AgregarUsuario;
import cliente.vistas.VistasJefe.AnteproyectosAprobados;
import cliente.vistas.VistasJefe.BuscarAnteproyecto;
import cliente.vistas.VistasJefe.ModificarEstadoAnteproyecto;
import java.rmi.RemoteException;
import cliente.vistas.VistasJefe.FrmVistaPrincialJefe;
import cliente.vistas.VistasJefe.ListaAnteproyectos;
import javax.swing.JOptionPane;
import servidor.sop_rmi.GestionAnteproyectosInt;
import servidor.sop_rmi.GestionUsuariosInt;

/**
 *
 * @author user
 */
public class ClienteDeObjetos {

    private static GestionUsuariosInt objRemotoUser;
    private static GestionAnteproyectosInt objRemotoAP;

    public static void main(String[] args) throws RemoteException {
        int numPuertoRMIRegistry;
        String direccionIpRMIRegistry;

        //System.out.println("Cual es la direccion ip donde se encuentra  el rmiregistry ");
        direccionIpRMIRegistry = "localhost";
        //System.out.println("Cual es el numero de puerto por el cual escucha el rmiregistry ");
        numPuertoRMIRegistry = 2020;

        objRemotoUser = (GestionUsuariosInt) cliente.utilidades.UtilidadesRegistroC.obtenerObjRemoto(direccionIpRMIRegistry, numPuertoRMIRegistry, "ObjetoRemotoUsuarios");
        
        ValidacionCredenciales validacion = new ValidacionCredenciales(objRemotoUser);
        System.out.println("Se realizo conexion con exito");
        if(validacion.validarCredenciales()){
            objRemotoAP = (GestionAnteproyectosInt) cliente.utilidades.UtilidadesRegistroC.obtenerObjRemoto(direccionIpRMIRegistry, numPuertoRMIRegistry, "ObjetoRemotoAnteproyectos");
            clienteModelo modelo = new clienteModelo(objRemotoAP, objRemotoUser);
            clienteControler controller = new clienteControler();
            FrmVistaPrincialJefe myView=new FrmVistaPrincialJefe(validacion.getNombreUsuario());
            
            BuscarAnteproyecto panelBuscarAnteproyecto= new BuscarAnteproyecto();
            AgregarUsuario panelAgrgarUsuario= new AgregarUsuario();
            AgregarAnteproyecto panelAgragarAnteproyecto= new AgregarAnteproyecto();
            ListaAnteproyectos panelListaAnproyecto= new ListaAnteproyectos();
            AsignarEvaluador panelAgregarEvaluador= new AsignarEvaluador();
            ModificarEstadoAnteproyecto panelCambiarConceptoAnteproyecto= new ModificarEstadoAnteproyecto();
            AnteproyectosAprobados panelAnteproyectosAprobados= new AnteproyectosAprobados();
            
            
            modelo.addObserver(myView);
            modelo.addObserver(panelBuscarAnteproyecto);
            modelo.addObserver(panelAgrgarUsuario);
            modelo.addObserver(panelAgragarAnteproyecto);
            modelo.addObserver(panelListaAnproyecto);
            modelo.addObserver(panelAgregarEvaluador);
            modelo.addObserver(panelCambiarConceptoAnteproyecto);
            modelo.addObserver(panelAnteproyectosAprobados);
            
            
            controller.addModel(modelo);
            controller.addView(myView);
            
            myView.addController(controller);
            panelBuscarAnteproyecto.addController(controller);
            panelAgragarAnteproyecto.addController(controller);
            panelListaAnproyecto.addController(controller);
            panelAgregarEvaluador.addController(controller);
            panelCambiarConceptoAnteproyecto.addController(controller);
            panelAgrgarUsuario.addController(controller);
            panelAnteproyectosAprobados.addControlller(controller);
            
            myView.agregarPaneles(panelAgrgarUsuario,
                    panelAgragarAnteproyecto,
                    panelListaAnproyecto, 
                    panelBuscarAnteproyecto,
                    panelAgregarEvaluador,
                    panelCambiarConceptoAnteproyecto,
                    panelAnteproyectosAprobados);
            myView.setVisible(true);
            
        
        }else{
            JOptionPane.showMessageDialog(null, "Error al vaidar las credenciales");
        }
        
        
    }

}
