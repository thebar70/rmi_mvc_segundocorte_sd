/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cliente.clienteObjetos;

import cliente.vistas.VistasJefe.FrmVistaPrincialJefe;
import java.awt.event.ActionEvent;
import servidor.dto.UsuarioDTO;

/**
 *
 * @author thebar70
 */
public class clienteControler implements java.awt.event.ActionListener {

    private clienteModelo modelo;
    private FrmVistaPrincialJefe vista;

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case "jefeBtnRegistrarUsuario":
                break;
            case "jefeBtnRegistrarAnteproyecto":
                modelo.ListaUsuarios();
                modelo.listaAnteproyectos();
                break;
            case "panelAgregarAnteproyectoRegistrar":
                modelo.registrarAnteproyecto(vista.getAnteproyectoDTO());
                break;
            case "jefeBuscarAnteproyecto":
                modelo.buscarAnteproyecto(vista.getCodigoAnteproyectoDTO());
                modelo.buscarEvaluadorEnAnteproyecto(vista.getCodigoAnteproyectoDTO());
                break;
            case "panelAgregarUsuarioRegistrar":
                modelo.registrarUsuario(vista.getUsuarioDTO());
                break;
            case "jefePanelListaAnteproyecto":
                modelo.listaAnteproyectos();
                break;
            case "AgregarEvaluadorJtextFieldCedulaEvaluador":
                modelo.ListaUsuarios();
                break;
            case "AgregarEvaluadorJtextFieldCodigoAnteproyecto":
                modelo.listaAnteproyectos();
                break;
            case "AsignarEvaluadorJbuttonAsignar":
                modelo.asignarEvaluador(vista.asignar_evaluador_getPosEvaluador1(),
                        vista.asignar_evaluador_getPosEvaluador2(),
                        vista.asignar_evaluador_getCodigoAnteproyecto());
                break;
            case "CambiarConceptoAnteproyectoJbuttonModificar":
                modelo.modificarConceptoAnteproyecto(vista.cambiar_concepto_anteproyecto_getCodigoAp());
                break;
            case "CambiarConceptoAnteproyectoJtextFieldCodigoAp":
                modelo.buscarAnteproyecto(vista.cambiar_concepto_anteproyecto_getCodigoApBuscado());
                modelo.buscarEvaluadorEnAnteproyecto(vista.cambiar_concepto_anteproyecto_getCodigoApBuscado());
                break;
            case "jButtonConsultarAprobados":
                modelo.listaAnteproyectos();
                break;
            case "jButtonListarAnteproyectos":
                modelo.listaAnteproyectos();
                break;
            default:
                break;
        }
    }

    public void addModel(clienteModelo m) {
        this.modelo = m;
    }

    public void addView(FrmVistaPrincialJefe v) {
        this.vista = v;
    }

}
