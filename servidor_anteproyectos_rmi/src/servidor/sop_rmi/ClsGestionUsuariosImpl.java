/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servidor.sop_rmi;

import java.io.IOException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import servidor.dto.IniciarSesionDTO;
import servidor.dto.UsuarioDTO;
import servidor.dao.UsuarioDAO;
import servidor.dao.IniciarSesionDAO;


public class ClsGestionUsuariosImpl extends UnicastRemoteObject implements GestionUsuariosInt {
    private UsuarioDAO infoUsuario;
    private IniciarSesionDAO infoInicioSesion;

    public ClsGestionUsuariosImpl() throws RemoteException{
        super();
        infoUsuario = new UsuarioDAO();
        infoInicioSesion = new IniciarSesionDAO();
    }

    @Override
    public int iniciarSesion(IniciarSesionDTO objSesion)throws RemoteException{
        IniciarSesionDAO objSesionDAO = new IniciarSesionDAO();
        UsuarioDTO admin = new UsuarioDTO("Daniel", 12345, "admin", "ingesis", 1);
        int tipoUsuario = -1;
        ArrayList<UsuarioDTO> listaUsuarios = null;
        ArrayList<UsuarioDTO> listaAdministrador = null;
        
        try {
            objSesionDAO.inicioFicheroAdmin();
            objSesionDAO.GenerarAdmin(admin);
            objSesionDAO.inicioFicheroUsuarios();
            
            listaAdministrador = objSesionDAO.getAdministrador();
            listaUsuarios = objSesionDAO.getUsuarios();
            
            for(int i = 0; i < listaAdministrador.size(); i++)
            {
                if(listaAdministrador.get(i).getUsuario().equalsIgnoreCase(objSesion.getUsuario())&& listaAdministrador.get(i).getContrasena().equals(objSesion.getContrasena()))
                {
                    tipoUsuario = listaAdministrador.get(i).getTipoUsuario();
                    break;
                }
            }
            for(int j = 0; j < listaUsuarios.size(); j++)
            {
                if(listaUsuarios.get(j).getUsuario().equalsIgnoreCase(objSesion.getUsuario())&& listaUsuarios.get(j).getContrasena().equals(objSesion.getContrasena()))
                {
                    tipoUsuario = listaUsuarios.get(j).getTipoUsuario();
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(ClsGestionUsuariosImpl.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ClsGestionUsuariosImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return tipoUsuario;
    }

    @Override
    public boolean registrarUsuario(UsuarioDTO objUsuario)throws RemoteException{
        System.out.println("Llamo a la funcionalidad registrarUsuario");
        boolean resultado = false;
        UsuarioDAO objUserDAO = new UsuarioDAO();
        try {
            objUserDAO.inicioFicheroUsuarios();
            objUserDAO.nuevoUsuario(objUsuario);
            resultado = true;
        } 
        catch (IOException ex) {
            resultado = false;
            Logger.getLogger(ClsGestionUsuariosImpl.class.getName()).log(Level.SEVERE, null, ex);
        } 
        catch (ClassNotFoundException ex) {
            resultado = false;
            Logger.getLogger(ClsGestionUsuariosImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return resultado;
    }

    @Override
    public ArrayList<UsuarioDTO> listaUsuarios() throws RemoteException {
        System.out.println("Llamo a la funcionalidad listaUsuarios");
        ArrayList<UsuarioDTO> listaUsuarios = new ArrayList<>();
        UsuarioDAO objUserDAO = new UsuarioDAO();
        try {
            objUserDAO.getFicheroUsuarios();
            listaUsuarios = objUserDAO.getUsuarios();
            System.out.println("FUNCIONO");
        } catch (IOException ex) {
            Logger.getLogger(ClsGestionUsuariosImpl.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ClsGestionUsuariosImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listaUsuarios;
    }
    
    
    
}
