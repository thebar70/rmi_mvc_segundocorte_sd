/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servidor.sop_rmi;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import servidor.dto.EvaluadorDTO;


public class CallbackImpl extends UnicastRemoteObject implements CallbackInt{

    private ArrayList<EvaluadorDTO> evaluadores; 
    public CallbackImpl() throws RemoteException {
        super();
        evaluadores = new ArrayList<>();
    }

    @Override
    public String notificarEvaluador(int codigoAP) throws RemoteException {
        String mensaje = ("Mensaje del servidor, Anteproyecto de : "+ codigoAP+" asigando");
        JOptionPane.showMessageDialog(null, "Se le ha asignado un anteproyecto\n"+codigoAP);
        System.out.println(mensaje);
        return mensaje;
    }
}
