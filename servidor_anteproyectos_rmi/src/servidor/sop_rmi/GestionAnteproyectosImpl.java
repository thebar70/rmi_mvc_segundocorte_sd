/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servidor.sop_rmi;

import java.io.IOException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import servidor.dto.AnteproyectoDTO;
import servidor.dao.AnteproyectoDAO;
import servidor.dto.EvaluadorDTO;


public class GestionAnteproyectosImpl extends UnicastRemoteObject implements GestionAnteproyectosInt {
    private AnteproyectoDAO objAPDAO = new AnteproyectoDAO();
    private ArrayList<CallbackInt> refEvaluadores;
    
    public GestionAnteproyectosImpl() throws RemoteException{
        super();
        objAPDAO = new AnteproyectoDAO();
        refEvaluadores= new ArrayList<>();
    }

    @Override
    public boolean registrarAnteproyectos(AnteproyectoDTO objAnteproyecto) throws RemoteException {
        System.out.println("Llamo a la funcionalidad registrarAnteproyectos");
        AnteproyectoDAO objApDAO = new AnteproyectoDAO();
        boolean respuesta = false;
        try {
            objApDAO.inicioFicheroAp();
            objApDAO.nuevoAP(objAnteproyecto);
            respuesta = true;
        } 
        catch (IOException ex) {
            respuesta = false;
            Logger.getLogger(GestionAnteproyectosImpl.class.getName()).log(Level.SEVERE, null, ex);
        } 
        catch (ClassNotFoundException ex) {
            respuesta = false;
            Logger.getLogger(GestionAnteproyectosImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        if(respuesta == true)
        {
            System.out.println("Registro AP exitoso");
        }
        else
        {
            System.out.println("Registro AP no fue Posible");
        }
        return true;
    }

    @Override
    public boolean asignarEvaluadores(EvaluadorDTO objEvaluador)throws RemoteException{
        System.out.println("Llamo a la funcionalidad asignarEvaluadores");
        AnteproyectoDAO objApDAO = new AnteproyectoDAO();
        boolean respuesta = false;
        try {
            objApDAO.inicioFicheroEvaluador();
            objApDAO.nuevoEvaluador(objEvaluador);
            notificar(objEvaluador);
            respuesta = true;
        } 
        catch (IOException ex) {
            respuesta = false;
            Logger.getLogger(GestionAnteproyectosImpl.class.getName()).log(Level.SEVERE, null, ex);
        } 
        catch (ClassNotFoundException ex) {
            respuesta = false;
            Logger.getLogger(GestionAnteproyectosImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        if(respuesta == true)
        {
            System.out.println("Asignacion exitosa");
        }
        else
        {
            System.out.println("La asignacion No Fue Posible");
        }
        return respuesta;
    }

    @Override
    public EvaluadorDTO buscarEvaluadores(int codigoAP) throws RemoteException {
        EvaluadorDTO EvaluadorAux = null;
        ArrayList<EvaluadorDTO> listaEval = new ArrayList<>();
        AnteproyectoDAO objApDAO = new AnteproyectoDAO();
        objApDAO.getFicheroEvaluadores();
        listaEval = objApDAO.getEvaluadores();
        for(int i = 0; i< listaEval.size(); i++)
        {
            if(listaEval.get(i).getCodigoAp() == codigoAP)
            {
                EvaluadorAux = listaEval.get(i);
                break;
            }
        }
        return EvaluadorAux;
    }
    
    @Override
    public AnteproyectoDTO buscarAnteproyecto(int codigoAP) throws RemoteException {
        System.out.println("Llamo a la funcionalidad buscarAnteproyecto");
        AnteproyectoDTO AnteproyectoAux = null;
        ArrayList<AnteproyectoDTO> listaAp = new ArrayList<>();
        AnteproyectoDAO objApDAO = new AnteproyectoDAO();
        try {
            objApDAO.getFicheroAnteproyectos();
            listaAp = objApDAO.getAnteproyectos();
            for(int i = 0; i< listaAp.size(); i++)
            {
                if(listaAp.get(i).getCodigoAP() == codigoAP)
                {
                    AnteproyectoAux = listaAp.get(i);
                    System.out.println("AP encontrado");
                    break;
                }
            }
        } 
        catch (IOException ex) {
            Logger.getLogger(GestionAnteproyectosImpl.class.getName()).log(Level.SEVERE, null, ex);
        } 
        catch (ClassNotFoundException ex) {
            Logger.getLogger(GestionAnteproyectosImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return AnteproyectoAux;
    }

    
     @Override
    public boolean ingresarConceptoEval(int codigoAP,int cedulaEval ,int conceptoAP) throws RemoteException {
        AnteproyectoDAO objApDAO = new AnteproyectoDAO();
        try {
            objApDAO.ingresarConceptoEvaluador(codigoAP, cedulaEval, conceptoAP);
        } catch (IOException ex) {
            Logger.getLogger(GestionAnteproyectosImpl.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(GestionAnteproyectosImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return true;
    }
    @Override
    public boolean modificarConcepto(int codigoAP) throws RemoteException {
        AnteproyectoDAO objApDAO = new AnteproyectoDAO();
        boolean bandera=false;
        try {
            bandera=objApDAO.jefeModificarConcepto(codigoAP);
        } catch (IOException ex) {
            Logger.getLogger(GestionAnteproyectosImpl.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(GestionAnteproyectosImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return bandera;
    }
    
    public void notificar(EvaluadorDTO objEvaluador) throws RemoteException
    {
        for (CallbackInt refEvaluadore : refEvaluadores) {
            
            refEvaluadore.notificarEvaluador(objEvaluador.getCodigoAp());
        }
    }

    @Override
    public void registrarRefEvaludor(CallbackInt refEvaluador) {
        this.refEvaluadores.add(refEvaluador);
    }
     @Override
    public ArrayList<AnteproyectoDTO> listarAnteproyectos() throws RemoteException {
        System.out.println("Llamo a la funcionalidad listarAnteproyectos");
        ArrayList<AnteproyectoDTO> listaAp = new ArrayList<>();
        AnteproyectoDAO objApDAO = new AnteproyectoDAO();
        try {
            objApDAO.getFicheroAnteproyectos();
            listaAp = objApDAO.getAnteproyectos();
        } 
        catch (IOException ex) {
            Logger.getLogger(GestionAnteproyectosImpl.class.getName()).log(Level.SEVERE, null, ex);
        } 
        catch (ClassNotFoundException ex) {
            Logger.getLogger(GestionAnteproyectosImpl.class.getName()).log(Level.SEVERE, null, ex);
        }

        return listaAp;
    }
}
