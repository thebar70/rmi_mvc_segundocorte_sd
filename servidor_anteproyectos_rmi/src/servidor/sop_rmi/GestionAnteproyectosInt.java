/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servidor.sop_rmi;

import servidor.dto.AnteproyectoDTO;
import servidor.dto.EvaluadorDTO;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

/**
 *
 * @author user
 */
public interface GestionAnteproyectosInt extends Remote{
    boolean registrarAnteproyectos(AnteproyectoDTO objAnteproyecto)throws RemoteException;
    boolean asignarEvaluadores(EvaluadorDTO objEvaluador)throws RemoteException;
    AnteproyectoDTO buscarAnteproyecto(int codigoAP)throws RemoteException;
    EvaluadorDTO buscarEvaluadores(int codigoAP)throws RemoteException;
    boolean modificarConcepto(int codigoAP)throws RemoteException;
    boolean ingresarConceptoEval(int codigoAP,int cedulaEval ,int conceptoAP)throws RemoteException;
    void registrarRefEvaludor(CallbackInt refEvaluador)throws RemoteException;
    ArrayList<AnteproyectoDTO> listarAnteproyectos()throws RemoteException;
}
