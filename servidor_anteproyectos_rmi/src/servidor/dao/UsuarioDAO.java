/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servidor.dao;

import servidor.dto.UsuarioDTO;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author user
 */
public class UsuarioDAO {
    
    private ArrayList<UsuarioDTO> usuarios;
    private UsuarioDTO user ;

    public UsuarioDAO() {
    }

    public ArrayList<UsuarioDTO> getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(ArrayList<UsuarioDTO> usuarios) {
        this.usuarios = usuarios;
    }

    public UsuarioDTO getUser() {
        return user;
    }

    public void setUser(UsuarioDTO user) {
        this.user = user;
    }
    
    public void inicioFicheroUsuarios() throws IOException, ClassNotFoundException
    {
        File  fichero = new File("usuarios.txt");
        if(fichero.exists())
        {
            System.out.println("Archivo: "+fichero.getName());
            getFicheroUsuarios();
        }
        else
        {
            System.out.println("Se ha creado el archivo: "+fichero.getName());
        }
    }
    
    public void nuevoUsuario(UsuarioDTO objUser) throws IOException, ClassNotFoundException
    {
        getFicheroUsuarios();
        usuarios.add(objUser);
        GuardarDatosUsuario();
    }
    
    public void getFicheroUsuarios() throws IOException, ClassNotFoundException
    {
        FileInputStream fis;
        ObjectInputStream ois;
        usuarios = new ArrayList<>();
        try{
            fis = new FileInputStream("usuarios.txt");
            ois = new ObjectInputStream(fis);
            usuarios = (ArrayList<UsuarioDTO>) ois.readObject();
        }
        catch(FileNotFoundException e){
            System.out.println("Error en la localizacion del archivo usuarios.txt");
        }
        catch(IOException e){
            System.out.println("Error en la manipulacion del archivo usuarios.txt");
        } 
        catch (ClassNotFoundException ex) {
            Logger.getLogger(AnteproyectoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }  
    
    public boolean GuardarDatosUsuario() throws IOException
    {
        boolean respuesta = false;
        
        File fichero = new File("usuarios.txt");
        FileOutputStream fos;
        ObjectOutputStream oos;
        try{
            fos = new FileOutputStream(fichero);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(usuarios);
            oos.close();
        }
        catch(FileNotFoundException e){
            System.out.println("Error en la localizacion del archivo");
        }
        catch(IOException e){
            System.out.println("Error en la manipulacion del archivo");
        }
        return respuesta;
    }
    
}
