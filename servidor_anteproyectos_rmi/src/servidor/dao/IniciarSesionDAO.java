/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servidor.dao;

import servidor.dto.IniciarSesionDTO;
import servidor.dto.UsuarioDTO;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author user
 */
public class IniciarSesionDAO {
    
    private ArrayList<UsuarioDTO> usuarios;
    private ArrayList<UsuarioDTO> administrador;
    private IniciarSesionDTO sesion;

    public IniciarSesionDAO() {
        usuarios = new ArrayList<>();
        administrador = new ArrayList<>();
        sesion = sesion;
    }

    public ArrayList<UsuarioDTO> getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(ArrayList<UsuarioDTO> usuarios) {
        this.usuarios = usuarios;
    }

    public ArrayList<UsuarioDTO> getAdministrador() {
        return administrador;
    }

    public void setAdministrador(ArrayList<UsuarioDTO> usuarios) {
        this.administrador = administrador;
    }

    public IniciarSesionDTO getSesion() {
        return sesion;
    }

    public void setSesion(IniciarSesionDTO sesion) {
        this.sesion = sesion;
    }
    
    public void inicioFicheroUsuarios() throws IOException, ClassNotFoundException
    {
        File  fichero = new File("usuarios.txt");
        if(fichero.exists())
        {
            System.out.println("Archivo: "+fichero.getName());
            getFicheroUsuarios();
        }
        else
        {
            System.out.println("Se ha creado el archivo: "+fichero.getName());
        }
    }
    
    public void inicioFicheroAdmin() throws IOException, ClassNotFoundException
    {
        File  fichero = new File("admin.txt");
        if(fichero.exists())
        {
            System.out.println("Archivo: "+fichero.getName());
            getFicheroUsuarios();
        }
        else
        {
            System.out.println("Se ha creado el archivo: "+fichero.getName());
        }
    }
    
    public void getFicheroAdmin() throws IOException, ClassNotFoundException
    {
        FileInputStream fis;
        ObjectInputStream ois;
        administrador = new ArrayList<>();
        try{
            fis = new FileInputStream("admin.txt");
            ois = new ObjectInputStream(fis);
            administrador = (ArrayList<UsuarioDTO>) ois.readObject();
        }
        catch(FileNotFoundException e){
            System.out.println("Error en la localizacion del archivo admin.txt");
        }
        catch(IOException e){
            System.out.println("Error en la manipulacion del archivo admin.txt");
        } 
        catch (ClassNotFoundException ex) {
            Logger.getLogger(IniciarSesionDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }  
    
    public void getFicheroUsuarios() throws IOException, ClassNotFoundException
    {
        FileInputStream fis;
        ObjectInputStream ois;
        usuarios = new ArrayList<>();
        try{
            fis = new FileInputStream("usuarios.txt");
            ois = new ObjectInputStream(fis);
            usuarios = (ArrayList<UsuarioDTO>) ois.readObject();
        }
        catch(FileNotFoundException e){
            System.out.println("Error en la localizacion del archivo usuarios.txt");
        }
        catch(IOException e){
            System.out.println("Error en la manipulacion del archivo usuarios.txt");
        } 
        catch (ClassNotFoundException ex) {
            Logger.getLogger(IniciarSesionDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    } 
    
    public void GenerarAdmin(UsuarioDTO objUser) throws IOException, ClassNotFoundException
    {
        getFicheroUsuarios();
        administrador.add(objUser);
        GuardarDatosAdmin();
    }
    
    public boolean GuardarDatosAdmin() throws IOException
    {
        boolean respuesta = false;
        File fichero = new File("admin.txt");
        FileOutputStream fos;
        ObjectOutputStream oos;
        try{
            fos = new FileOutputStream(fichero);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(administrador);
            oos.close();
        }
        catch(FileNotFoundException e){
            System.out.println("Error en la localizacion del archivo");
        }
        catch(IOException e){
            System.out.println("Error en la manipulacion del archivo");
        }
        return respuesta;
    }
}
