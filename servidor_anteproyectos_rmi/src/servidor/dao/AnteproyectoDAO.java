/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servidor.dao;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import servidor.dto.AnteproyectoDTO;
import servidor.dto.EvaluadorDTO;
/**
 *
 * @author user
 */
public class AnteproyectoDAO { 
    
    private AnteproyectoDTO ap ;
    private ArrayList<AnteproyectoDTO> anteproyectos;
    private ArrayList<EvaluadorDTO> evaluadores;
        
    public AnteproyectoDAO() {
        ap = ap;
        anteproyectos = new ArrayList<>();
        evaluadores = new ArrayList<>();
    }

    public ArrayList<EvaluadorDTO> getEvaluadores() {
        return evaluadores;
    }

    public AnteproyectoDTO getAp() {
        return ap;
    }

    public ArrayList<AnteproyectoDTO> getAnteproyectos() {
        return anteproyectos;
    }
    
    public void inicioFicheroAp() throws IOException, ClassNotFoundException
    {
        File  fichero = new File("anteproyectos.txt");
        if(fichero.exists())
        {
            System.out.println("Archivo: "+fichero.getName());
            getFicheroAnteproyectos();
        }
        else
        {
            System.out.println("Se ha creado el archivo: "+fichero.getName());
        }
    }
    
    public void inicioFicheroEvaluador() throws IOException, ClassNotFoundException
    {
        File  fichero = new File("evaluadores.txt");
        if(fichero.exists())
        {
            System.out.println("Archivo: "+fichero.getName());
            getFicheroEvaluadores();
        }
        else
        {
            System.out.println("Se ha creado el archivo: "+fichero.getName());
        }
    }
    
    public void nuevoAP(AnteproyectoDTO objAp) throws IOException, ClassNotFoundException
    {
        getFicheroAnteproyectos();
        anteproyectos.add(objAp);
        GuardarDatosAP();
    }
    
    public void nuevoEvaluador(EvaluadorDTO objEval) throws IOException, ClassNotFoundException
    {
        getFicheroEvaluadores();
        boolean bandera = false;
        int i = 0;
        for(i = 0; i < evaluadores.size(); i++)
        {
            if(objEval.getCodigoAp() == evaluadores.get(i).getCodigoAp())
            {
                bandera = true;
                break;
            }
        }
        if(bandera == false)
        {
            evaluadores.add(objEval);
            modificarEstado(objEval.getCodigoAp(), 2);
            GuardarDatosEvaluador();
        }
        else
        {
            GuardarDatosEvaluador(); 
        }
    }
    
    public boolean GuardarDatosAP() throws IOException
    {
        boolean respuesta = false;
        
        File fichero = new File("anteproyectos.txt");
        FileOutputStream fos;
        ObjectOutputStream oos;
        try{
            fos = new FileOutputStream(fichero);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(anteproyectos);
            oos.close();
        }
        catch(FileNotFoundException e){
            System.out.println("Error en la localizacion del archivo");
        }
        catch(IOException e){
            System.out.println("Error en la manipulacion del archivo");
        }
        return respuesta;
    }
    
    public boolean GuardarDatosEvaluador()
    {
        boolean respuesta = false;
        File fichero = new File("evaluadores.txt");
        FileOutputStream fos;
        ObjectOutputStream oos;
        try{
            fos = new FileOutputStream(fichero);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(evaluadores);
            oos.close();
        }
        catch(FileNotFoundException e){
            System.out.println("Error en la localizacion del archivo");
        }
        catch(IOException e){
            System.out.println("Error en la manipulacion del archivo");
        }
        return respuesta;
    }
    
    public void getFicheroAnteproyectos() throws IOException, ClassNotFoundException
    {
        FileInputStream fis;
        ObjectInputStream ois;
        anteproyectos = new ArrayList<>();
        try{
            fis = new FileInputStream("anteproyectos.txt");
            ois = new ObjectInputStream(fis);
            anteproyectos = (ArrayList<AnteproyectoDTO>) ois.readObject();
        }
        catch(FileNotFoundException e){
            System.out.println("Error en la localizacion del archivo anteproyectos");
        }
        catch(IOException e){
            System.out.println("Error en la manipulacion del archivo anteproyectos");
        } 
        catch (ClassNotFoundException ex) {
            Logger.getLogger(AnteproyectoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    
    
    public void getFicheroEvaluadores()
    {
        FileInputStream fis;
        ObjectInputStream ois;
        evaluadores = new ArrayList<>();
        try{
            fis = new FileInputStream("evaluadores.txt");
            ois = new ObjectInputStream(fis);
            evaluadores = (ArrayList<EvaluadorDTO>) ois.readObject();
        }
        catch(FileNotFoundException e){
            System.out.println("Error en la localizacion del archivo evaluadores.txt");
        }
        catch(IOException e){
            System.out.println("Error en la manipulacion del archivo evaluadores.txt");
        } 
        catch (ClassNotFoundException ex) {
            Logger.getLogger(AnteproyectoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    
    
    public boolean ingresarConceptoEvaluador(int codigoAp, int cedulaEval, int concepto) throws IOException, ClassNotFoundException
    {
        boolean bandera = false;
        inicioFicheroEvaluador();
        getFicheroEvaluadores();
        
        if(concepto == 1 || concepto == 2)
        {
            for(int i = 0; i < evaluadores.size(); i++)
            {
                if(evaluadores.get(i).getCodigoAp()==codigoAp && evaluadores.get(i).getCedulaEval1() == cedulaEval)
                {
                    System.out.println("entro por el primer evaluador");
                    evaluadores.get(i).setConceptoEval_1(concepto);
                    evaluadores.get(i).setFechaRevision_1(LocalDate.now());
                    modificarNumeroRevision(codigoAp);
                    modificarEstado(codigoAp, 3);
                    bandera = true;
                    break;
                }
                else if (evaluadores.get(i).getCodigoAp()==codigoAp && evaluadores.get(i).getCedulaEval2() == cedulaEval)
                {
                    System.out.println("entro por el segundo evaluador");
                    evaluadores.get(i).setConceptoEval_2(concepto);
                    evaluadores.get(i).setFechaRevision_2(LocalDate.now());
                    modificarNumeroRevision(codigoAp);
                    modificarEstado(codigoAp, 3);
                    bandera = true;
                    break;
                }
            }
        }
        GuardarDatosEvaluador();
        return bandera;
    }
    public void modificarNumeroRevision(int codigoAp) throws IOException, ClassNotFoundException
    {
        inicioFicheroAp();
        getFicheroAnteproyectos();
        int numRevision = 0;
        for(int i = 0; i < anteproyectos.size(); i++)
        {
            if(codigoAp == anteproyectos.get(i).getCodigoAP())
            {
                numRevision = anteproyectos.get(i).getNumRevision();
                numRevision = numRevision+1;
                anteproyectos.get(i).setNumRevision(numRevision);
                break;
            }
        }
        GuardarDatosAP();
    }
    
    public boolean jefeModificarConcepto(int codigoAP) throws IOException, ClassNotFoundException
    {
        boolean bandera = false;
        boolean permitirCambio = false;
        
        inicioFicheroAp();
        inicioFicheroEvaluador();
        getFicheroAnteproyectos();
        getFicheroEvaluadores();
       
        int posAp = 0;
        for(int i=0; i < anteproyectos.size(); i++)
        {
            if(anteproyectos.get(i).getCodigoAP() == codigoAP)
            {
                posAp = i;
                for(int j = 0; j < evaluadores.size(); j++)
                {
                    if(evaluadores.get(i).getCodigoAp() == codigoAP)
                    {
                        if(evaluadores.get(i).getConceptoEval_1()==1 && evaluadores.get(i).getConceptoEval_2()==1)
                        {
                            permitirCambio = true;
                            bandera=true;
                            
                        }
                        else
                        {
                            permitirCambio = false;
                        }
                        break;
                    }
                }
                break;
            }
        }
        if(permitirCambio ==  true)
        {
            int numRevision = anteproyectos.get(posAp).getNumRevision();
            anteproyectos.get(posAp).setConcepto(1);
            anteproyectos.get(posAp).setNumRevision(numRevision+1);
            modificarEstado(codigoAP, 4);
        }
        else
        {
            anteproyectos.get(posAp).setConcepto(2);
        }
        int numRevision = anteproyectos.get(posAp).getNumRevision();
        anteproyectos.get(posAp).setConcepto(1);
        GuardarDatosAP();
        return bandera;
    }
    
    public void modificarEstado(int codigoAp, int estado) throws IOException, ClassNotFoundException
    {
        inicioFicheroAp();
        getFicheroAnteproyectos();
        for(int i = 0; i < anteproyectos.size(); i++)
        {
            if(codigoAp == anteproyectos.get(i).getCodigoAP())
            {
               
                anteproyectos.get(i).setEstado(estado);
                break;
            }
        }
        GuardarDatosAP();
    }
   
}
